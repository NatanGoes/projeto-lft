package visitor;

import For.ForCompleto;
import begin.BeginProg;
import chamarFuncao.ChamadaFunc;
import conteudoCorpo.ChamaFunc;
import conteudoCorpo.Comandos;
import conteudoCorpo.DecVariaveis;
import conteudoPrint.ContPrint;
import corpoComandos.ComandosFor;
import corpoComandos.ComandosIf;
import corpoComandos.ComandosIgual;
import corpoComandos.ComandosImpr;
import corpoFuncao.CorpoFuncao;
import corpoGeral.CompoundConteudoCorpo;
import darComandos.DarCmd;
import declararVariaveis.IdExp;
import declararVariaveis.IdFunc;
import declararVariaveis.IdVetor;
import exp.ExpAnd;
import exp.ExpDiv;
import exp.ExpMul;
import exp.ExpOperando;
import exp.ExpOu;
import exp.ExpPar;
import exp.ExpSub;
import exp.ExpSum;
import exp.ExpValor;
import funcao.Funcao;
import funcoes.CompoundFunc;
import ifElse.If;
import impressao.Impr;
import iteracao.Iter;
import listaParametros.CompoundParam;
import listaParametrosChamada.ListaChamada;
import listaValores.CompoundValor;
import main.FuncMain;
import operando.DiferenteExp;
import operando.DivIgualExp;
import operando.EqualExp;
import operando.MaiorIgualExp;
import operando.MaiorqExp;
import operando.MenorIgualExp;
import operando.MenorSubExp;
import operando.MenorqExp;
import operando.RestoIgualExp;
import parametro.Parametro;
import print.CmdPrint;
import println.CmdPrintln;
import programa.MainFuncoes;
import valor.ValorBoolean;
import valor.ValorFloat;
import valor.ValorIdentificador;
import valor.ValorInteiro;
import valor.ValorString;
import virgulaID.VirgulaIdentificador;
import virgulaSTR.VirgulaString;

public class PPVisitor extends AbstractVisitor{
	
	public Object visit(BeginProg bp) {
		System.out.print("packge main");
		bp.p.accept(this);
		return null;
	}
	
	public Object visit(MainFuncoes mf) {
		mf.m.accept(this);
		if(mf.f!=null) {
			mf.f.accept(this);
		}
		return null;
	}
	public Object visit(CompoundFunc cf) {
		cf.f.accept(this);
		if(cf.lf!=null) {
			cf.lf.accept(this);
		}
		return null;
	}
	public Object visit(Funcao f) {
		System.out.print("func "+f.nome+" (");
		if(f.listaParam!=null) {
			f.listaParam.accept(this);
		}
		System.out.print(") ");
		if(f.tipoRetorno!=null) {
			System.out.print(f.tipoRetorno.intern());
		}
		System.out.print("{\n");
		if(f.corpo!=null) {
			f.corpo.accept(this);
		}
		System.out.println("}");
		return null;
	}
	public Object visit(CompoundParam cp) {
		cp.p.accept(this);
		if(cp.lp!=null) {
			System.out.print(", ");
			cp.lp.accept(this);
		}
		return null;
	}
	public Object visit(Parametro p) {
		System.out.print(p.nome + " "+p.t);
		return null;
	}
	public Object visit(CorpoFuncao cf) {
		cf.cg.accept(this);
		if(cf.retorno!=null) {
			System.out.println("return ");
			cf.retorno.accept(this);
		}
		return null;
	}
	public Object visit(CompoundConteudoCorpo lc) {
		lc.cp.accept(this);
		if(lc.cg!=null) {
			lc.cg.accept(this);
		}
		return null;
	}
	public Object visit(DecVariaveis dv) {
		
		return null;
	}
	public Object visit(Comandos cm) {
		return null;
	}
	public Object visit(ChamaFunc cf) {
		return null;
	}
	public Object visit(IdExp ie) {
		return null;
	}
	public Object visit(IdVetor iv) {
		return null;
	}
	public Object visit(IdFunc fi) {
		return null;
	}
	public Object visit(CompoundValor cv) {
		return null;
	}
	public Object visit(ChamadaFunc cf) {
		return null;
	}
	public Object visit(ValorInteiro vi) {
		return null;
	}
	public Object visit(ValorBoolean vb) {
		return null;
	}
	public Object visit(ValorFloat vf) {
		return null;
	}
	public Object visit(ValorString vs) {
		return null;
	}
	public Object visit(ValorIdentificador vi) {
		return null;
	}
	public Object visit(FuncMain fmain) {
		return null;
	}
	public Object visit(VirgulaString vs) {
		return null;
	}
	public Object visit(VirgulaIdentificador vi) {
		return null;
	}
	public Object visit(ContPrint cp) {
		return null;
	}
	public Object visit(CmdPrint cp) {
		return null;
	}
	public Object visit(CmdPrintln cp) {
		return null;
	}
	public Object visit(Impr i) {
		return null;
	}
	public Object visit(Iter i) {
		return null;
	}
	public Object visit(ForCompleto f) {
		return null;
	}
	public Object visit(If f) {
		return null;
	}
	public Object visit(ComandosIf c) {
		return null;
	}
	public Object visit(ComandosFor c) {
		return null;
	}
	public Object visit(ComandosImpr c) {
		return null;
	}
	public Object visit(ComandosIgual c) {
		return null;
	}
	public Object visit(DarCmd dc) {
		return null;
	}
	public Object visit(ListaChamada l) {
		return null;
	}
	public Object visit(ExpAnd e) {
		return null;
	}
	public Object visit(ExpDiv e) {
		return null;
	}
	public Object visit(ExpMul e) {
		return null;
	}
	public Object visit(ExpOperando e) {
		return null;
	}
	public Object visit(ExpOu e) {
		return null;
	}
	public Object visit(ExpPar e) {
		return null;
	}
	public Object visit(ExpSub e) {
		return null;
	}
	public Object visit(ExpSum e) {
		return null;
	}
	public Object visit(ExpValor e) {
		return null;
	}
	public Object visit(EqualExp e) {
		return null;
	}
	public Object visit(MenorIgualExp e) {
		return null;
	}
	public Object visit(MenorqExp e) {
		return null;
	}
	public Object visit(MenorSubExp e) {
		return null;
	}
	public Object visit(MaiorqExp e) {
		return null;
	}
	public Object visit(MaiorIgualExp e) {
		return null;
	}
	public Object visit(DivIgualExp e) {
		return null;
	}
	public Object visit(RestoIgualExp e) {
		return null;
	}
	public Object visit(DiferenteExp e) {
		return null;
	}

}
