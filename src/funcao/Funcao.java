package funcao;

import corpoFuncao.corpoFuncao;
import listaParametros.listaParametros;
import tipo.tipo;
import visitor.AbstractVisitor;

public class Funcao extends funcao{
	
	public String nome;
	public listaParametros listaParam;
	public String tipoRetorno;
	public corpoFuncao corpo;
	
	public Funcao(String nome, listaParametros listaParam, String tipoRetorno, corpoFuncao corpo) {
		this.nome = nome;
		this.listaParam = listaParam;
		this.tipoRetorno = tipoRetorno;
		this.corpo = corpo;
	}

	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
	
}
