package valor;

import visitor.AbstractVisitor;

public class ValorFloat extends valor{
	
	public float valor;

	public ValorFloat(float valor) {
		this.valor = valor;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
