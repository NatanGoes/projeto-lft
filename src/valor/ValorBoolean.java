package valor;

import visitor.AbstractVisitor;

public class ValorBoolean extends valor{
	
	public boolean valor;

	public ValorBoolean(boolean valor) {
		this.valor = valor;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
