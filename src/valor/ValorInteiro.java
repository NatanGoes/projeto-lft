package valor;

import visitor.AbstractVisitor;

public class ValorInteiro extends valor{
	
	public int valor;

	public ValorInteiro(int valor) {
		this.valor = valor;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
