package valor;

import visitor.AbstractVisitor;

public class ValorIdentificador extends valor{
	
	public String valor;

	public ValorIdentificador(String valor) {
		super();
		this.valor = valor;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
