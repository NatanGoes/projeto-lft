package valor;

import visitor.AbstractVisitor;

public class ValorString extends valor{
	
	public String valor;

	public ValorString(String valor) {
		this.valor = valor;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
