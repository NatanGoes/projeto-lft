package listaValores;

import valor.valor;
import visitor.AbstractVisitor;

public class CompoundValor extends listaValores{
	
	public valor v;
	public listaValores lv;
	
	public CompoundValor(valor v, listaValores lv) {
		this.v = v;
		this.lv = lv;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
