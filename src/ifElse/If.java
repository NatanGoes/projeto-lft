package ifElse;

import ifElse.ifElse;
import visitor.AbstractVisitor;
import exp.exp;
import corpoFuncao.corpoFuncao;

public class If extends ifElse{
	public exp e;
	public corpoFuncao cf, cf1;
	
	
	public If(exp e, corpoFuncao cf, corpoFuncao cf1) {
		this.e = e;
		this.cf = cf;
		this.cf1 = cf1;
	}


	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
