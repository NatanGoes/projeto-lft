package impressao;

import impressao.impressao;
import visitor.AbstractVisitor;
import print.print;
import println.println;

public class Impr extends impressao{
	public print p;
	public println pl;
	
	public Impr(print p, println pl) {
		this.p = p;
		this.pl = pl;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}	
	
}
