package operando;

import operando.operando;
import exp.exp;
import visitor.AbstractVisitor;

public class EqualExp extends operando{
	public exp e1, e2;

	public EqualExp(exp exp1, exp exp2) {
		this.e1 = exp1;
		this.e2 = exp2;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
