package operando;

import exp.exp;
import operando.operando;
import visitor.AbstractVisitor;

public class DivIgualExp extends operando{
	public exp e1, e2;
	
		
	public DivIgualExp(exp e1, exp e2) {
		this.e1 = e1;
		this.e2 = e2;
	}

	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
