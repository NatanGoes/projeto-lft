package listaParametrosChamada;

import listaParametrosChamada.listaParametrosChamada;
import visitor.AbstractVisitor;
import valor.valor;

public class ListaChamada extends listaParametrosChamada{
	public valor v;
	public listaParametrosChamada l;

	
	public ListaChamada(valor v, listaParametrosChamada l) {
		this.v = v;
		this.l = l;
	}


	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
