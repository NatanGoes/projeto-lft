package corpoFuncao;

import corpoGeral.corpoGeral;
import valor.valor;
import visitor.AbstractVisitor;

public class CorpoFuncao extends corpoFuncao{
	
	public corpoGeral cg;
	public valor retorno;
	
	public CorpoFuncao(corpoGeral cg, valor retorno) {
		this.cg = cg;
		this.retorno = retorno;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
