package virgulaSTR;

import virgulaSTR.virgulaSTR;
import visitor.AbstractVisitor;
import virgulaID.virgulaID;

public class VirgulaString extends virgulaSTR{
	public virgulaID vi;
	public String str_literal;
	
	
	public VirgulaString(virgulaID vi, String str_literal) {
		this.vi = vi;
		this.str_literal= str_literal;
	}


	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}	
}
