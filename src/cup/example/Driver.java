package cup.example;

import java.io.File;
import java.io.FileInputStream;
import java_cup.runtime.*;

class Driver {

	public static void main(String[] args) throws Exception {
		File f = new File("input.txt");
		FileInputStream i = new FileInputStream(f);
		Lexer lex = new Lexer(i);
		//Symbol symb = lex.next_token();
		/*while(symb.sym != sym.EOF) {
			System.out.println(symb.sym +"-"+ sym.terminalNames[symb.sym]);
			symb = lex.next_token();
		}*/
		
		Parser p = new Parser(lex);
		p.debug_parse();
	}
	
}