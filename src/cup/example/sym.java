
//----------------------------------------------------
// The following code was generated by CUP v0.11b 20141204 (SVN rev 60)
//----------------------------------------------------

package cup.example;

/** CUP generated interface containing symbol constants. */
public interface sym {
  /* terminals */
  public static final int ABRECOLCHETE = 27;
  public static final int STRING_LITERAL = 6;
  public static final int MENORQ = 32;
  public static final int DIFERENTE = 38;
  public static final int MAIORQ = 33;
  public static final int RESTOIGUAL = 41;
  public static final int INT = 2;
  public static final int PRINTLN = 16;
  public static final int DIVISAOIGUAL = 40;
  public static final int MAIORIGUAL = 37;
  public static final int FOR = 12;
  public static final int VIRGULA = 21;
  public static final int IDENTIFICADOR = 7;
  public static final int MENORIGUAL = 34;
  public static final int AND = 29;
  public static final int IGUAL = 20;
  public static final int OU = 31;
  public static final int BOOL = 4;
  public static final int MAIS = 18;
  public static final int MAIN = 13;
  public static final int IF = 10;
  public static final int MENORSUB = 36;
  public static final int EOF = 0;
  public static final int RETURN = 17;
  public static final int EQUAL = 30;
  public static final int TRUE = 44;
  public static final int error = 1;
  public static final int ABRECHAVE = 25;
  public static final int MENOS = 19;
  public static final int ABREPARENTESE = 23;
  public static final int DIVISAO = 39;
  public static final int NUM_INTEIRO = 42;
  public static final int ELSE = 11;
  public static final int FUNC = 9;
  public static final int PACKAGE = 14;
  public static final int FECHAPARENTESE = 24;
  public static final int FECHACHAVE = 26;
  public static final int FLOAT = 3;
  public static final int PONTO_E_VIRGULA = 22;
  public static final int ATRIBUICAO = 8;
  public static final int STRING = 5;
  public static final int NUM_FLOAT = 43;
  public static final int FECHACOLCHETE = 28;
  public static final int FALSE = 45;
  public static final int VEZES = 35;
  public static final int PRINT = 15;
  public static final String[] terminalNames = new String[] {
  "EOF",
  "error",
  "INT",
  "FLOAT",
  "BOOL",
  "STRING",
  "STRING_LITERAL",
  "IDENTIFICADOR",
  "ATRIBUICAO",
  "FUNC",
  "IF",
  "ELSE",
  "FOR",
  "MAIN",
  "PACKAGE",
  "PRINT",
  "PRINTLN",
  "RETURN",
  "MAIS",
  "MENOS",
  "IGUAL",
  "VIRGULA",
  "PONTO_E_VIRGULA",
  "ABREPARENTESE",
  "FECHAPARENTESE",
  "ABRECHAVE",
  "FECHACHAVE",
  "ABRECOLCHETE",
  "FECHACOLCHETE",
  "AND",
  "EQUAL",
  "OU",
  "MENORQ",
  "MAIORQ",
  "MENORIGUAL",
  "VEZES",
  "MENORSUB",
  "MAIORIGUAL",
  "DIFERENTE",
  "DIVISAO",
  "DIVISAOIGUAL",
  "RESTOIGUAL",
  "NUM_INTEIRO",
  "NUM_FLOAT",
  "TRUE",
  "FALSE"
  };
}

