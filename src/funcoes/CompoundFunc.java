package funcoes;

import funcao.funcao;
import visitor.AbstractVisitor;

public class CompoundFunc extends funcoes{
	
	public funcao f;
	public funcoes lf;
	
	public CompoundFunc(funcao f,funcoes lf) {
		this.f = f;
		this.lf = lf;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
