package For;

import visitor.AbstractVisitor;

public abstract class For {
	public abstract Object accept(AbstractVisitor av);
}
