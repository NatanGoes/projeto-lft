package For;

import For.For;
import visitor.AbstractVisitor;
import operando.operando;
import iteracao.iteracao;
import corpoGeral.corpoGeral;

public class ForCompleto extends For{
	public String id, id1, id2;
	public Integer num, num1;
	public String o;
	public iteracao i;
	public corpoGeral cg;
	


	public ForCompleto(String id, String id1, String id2, Integer num, Integer num1, String o, iteracao i, corpoGeral cg) {
		this.id = id;
		this.id1 = id1;
		this.id2 = id2;
		this.num = num;
		this.num1 = num1;
		this.o = o;
		this.i = i;
		this.cg = cg;
	}



	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
