package virgulaID;

import virgulaID.virgulaID;
import virgulaSTR.virgulaSTR;
import visitor.AbstractVisitor;

public class VirgulaIdentificador extends virgulaID{
	public String id;
	public virgulaSTR vs;
	
	
	
	public VirgulaIdentificador(String id, virgulaSTR vs) {
		this.id = id;
		this.vs = vs;
	}



	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
