package main;

import main.main;
import visitor.AbstractVisitor;
import corpoGeral.corpoGeral;

public class FuncMain extends main{
	public corpoGeral cg;
		
	
	public FuncMain(corpoGeral cg) {
		this.cg = cg;
	}


	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
