package declararVariaveis;

import chamarFuncao.chamarFuncao;
import visitor.AbstractVisitor;

public class IdFunc extends declararVariaveis{
	
	public String nome;
	public chamarFuncao cf;
	
	public IdFunc(String nome, chamarFuncao cf) {
		this.nome = nome;
		this.cf = cf;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
