package declararVariaveis;

import listaValores.listaValores;
import tipo.tipo;
import visitor.AbstractVisitor;

public class IdVetor extends declararVariaveis{
	
	public String nome;
	public String t;
	public listaValores lv;
	
	public IdVetor(String nome, String t, listaValores lv) {
		this.nome = nome;
		this.t = t;
		this.lv = lv;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
