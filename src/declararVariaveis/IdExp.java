package declararVariaveis;

import exp.exp;
import visitor.AbstractVisitor;

public class IdExp extends declararVariaveis{
	
	public String nome;
	public exp e;
	
	public IdExp(String nome, exp e) {
		this.nome = nome;
		this.e = e;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
