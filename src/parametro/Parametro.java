package parametro;

import tipo.tipo;
import visitor.AbstractVisitor;

public class Parametro extends parametro{
	
	public String nome;
	public String t;
	
	public Parametro(String nome, String t) {
		this.nome = nome;
		this.t = t;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
