package println;

import println.println;
import visitor.AbstractVisitor;
import conteudoPrint.conteudoPrint;

public class CmdPrintln extends println{
	public conteudoPrint cp;
	
	
	public CmdPrintln(conteudoPrint cp) {
		this.cp = cp;
	}


	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
