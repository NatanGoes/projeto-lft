package corpoGeral;

import conteudoCorpo.conteudoCorpo;
import visitor.AbstractVisitor;

public class CompoundConteudoCorpo extends corpoGeral{
	
	public conteudoCorpo cp;
	public corpoGeral cg;
	
	public CompoundConteudoCorpo(conteudoCorpo cp, corpoGeral cg) {
		this.cp = cp;
		this.cg = cg;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}