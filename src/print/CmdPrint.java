package print;

import print.print;
import visitor.AbstractVisitor;
import conteudoPrint.conteudoPrint;

public class CmdPrint extends print{
	public conteudoPrint cp;
	
	
	public CmdPrint(conteudoPrint cp) {
		this.cp = cp;
	}


	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
