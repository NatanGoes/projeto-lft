package conteudoCorpo;

import declararVariaveis.declararVariaveis;
import visitor.AbstractVisitor;

public class DecVariaveis extends conteudoCorpo{
	
	public declararVariaveis dv;

	public DecVariaveis(declararVariaveis dv) {
		this.dv = dv;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
