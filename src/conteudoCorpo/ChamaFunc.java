package conteudoCorpo;

import chamarFuncao.chamarFuncao;
import visitor.AbstractVisitor;

public class ChamaFunc extends conteudoCorpo{

		public chamarFuncao cf;
		
		public ChamaFunc(chamarFuncao cf) {
			this.cf = cf;
		}
		
		@Override
		public Object accept(AbstractVisitor av) {
			av.visit(this);
			return null;
		}
	
}
