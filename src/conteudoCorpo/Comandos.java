package conteudoCorpo;

import darComandos.darComandos;
import visitor.AbstractVisitor;

public class Comandos extends conteudoCorpo{
	
	public darComandos dc;

	public Comandos(darComandos dc) {
		this.dc = dc;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
