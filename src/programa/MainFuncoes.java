package programa;

import funcoes.funcoes;
import main.main;
import visitor.AbstractVisitor;

public class MainFuncoes extends programa{
	
	public main m;
	public funcoes f;
	
	public MainFuncoes(main m, funcoes f) {
		this.m = m;
		this.f = f;
	}

	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
