package chamarFuncao;

import listaParametrosChamada.listaParametrosChamada;
import visitor.AbstractVisitor;

public class ChamadaFunc extends chamarFuncao{
	
	public String nome;
	public listaParametrosChamada lp;
	
	public ChamadaFunc(String nome, listaParametrosChamada lp) {
		this.nome = nome;
		this.lp = lp;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}

}
