package begin;

import visitor.AbstractVisitor;
import programa.programa;

public class BeginProg extends begin{
	
	public programa p;

	public BeginProg(programa p) {
		this.p = p;
	}

	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
	
}
