package conteudoPrint;

import conteudoPrint.conteudoPrint;
import virgulaSTR.virgulaSTR;
import virgulaID.virgulaID;
import visitor.AbstractVisitor;


public class ContPrint extends conteudoPrint{
	public String str_literal, id;
	public virgulaSTR vs;
	public virgulaID vi;
	
	
	public ContPrint(String str_literal, String id, virgulaSTR vs, virgulaID vi) {
		this.str_literal = str_literal;
		this.id = id;
		this.vs = vs;
		this.vi = vi;
	}


	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
