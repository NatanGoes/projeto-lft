package exp;

import exp.exp;
import visitor.AbstractVisitor;

public class ExpOperando extends exp{
	public exp e1, e2;
	public String o;
	
	
	
	public ExpOperando(exp e1, exp e2, String o) {
		this.e1 = e1;
		this.e2 = e2;
		this.o = o;
	}



	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
