package exp;

import exp.exp;
import visitor.AbstractVisitor;

public class ExpPar extends exp{
	public exp e;
	
		
	public ExpPar(exp e) {
		this.e = e;
	}


	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
