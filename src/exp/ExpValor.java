package exp;

import exp.exp;
import valor.valor;
import visitor.AbstractVisitor;

public class ExpValor extends exp{
	public valor v;
	
		
	public ExpValor(valor v) {
		this.v = v;
	}

	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
