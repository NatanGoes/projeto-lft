package exp;

import exp.exp;
import visitor.AbstractVisitor;

public class ExpOu extends exp{
	public exp e1, e2;

	public ExpOu(exp e1, exp e2) {
		this.e1 = e1;
		this.e2 = e2;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
