package listaParametros;

import parametro.parametro;
import visitor.AbstractVisitor;

public class CompoundParam extends listaParametros{
	
	public parametro p;
	public listaParametros lp;
	
	public CompoundParam(parametro p, listaParametros lp) {
		this.p = p;
		this.lp = lp;
	}
	
	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
