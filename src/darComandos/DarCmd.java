package darComandos;

import darComandos.darComandos;
import corpoComandos.corpoComandos;
import visitor.AbstractVisitor;

public class DarCmd extends darComandos{
	public darComandos dc;
	public corpoComandos cc;
	
	
	public DarCmd(darComandos dc, corpoComandos cc) {
		this.dc = dc;
		this.cc = cc;
	}



	@Override
	public Object accept(AbstractVisitor av) {
		av.visit(this);
		return null;
	}
}
