package cup.example;
import java_cup.runtime.ComplexSymbolFactory;
import java_cup.runtime.ComplexSymbolFactory.Location;
import java_cup.runtime.Symbol;
import java.lang.*;
import java.io.InputStreamReader;

%%

%class Lexer
%implements sym
%public
%unicode
%line
%column
%cup
%char
%{

	StringBuilder string = new StringBuilder();
	
    public Lexer(ComplexSymbolFactory sf, java.io.InputStream is){
		this(is);
        symbolFactory = sf;
    }
	public Lexer(ComplexSymbolFactory sf, java.io.Reader reader){
		this(reader);
        symbolFactory = sf;
    }
    
    private StringBuffer sb;
    private ComplexSymbolFactory symbolFactory = new ComplexSymbolFactory();
    private int csline,cscolumn;

    public Symbol symbol(String name, int code){
		return symbolFactory.newSymbol(name, code,
						new Location(yyline+1,yycolumn+1, yychar), // -yylength()
						new Location(yyline+1,yycolumn+yylength(), yychar+yylength())
				);
    }
    public Symbol symbol(String name, int code, String lexem){
	return symbolFactory.newSymbol(name, code, 
						new Location(yyline+1, yycolumn +1, yychar), 
						new Location(yyline+1,yycolumn+yylength(), yychar+yylength()), lexem);
    }
    
    protected void emit_warning(String message){
    	System.out.println("scanner warning: " + message + " at : 2 "+ 
    			(yyline+1) + " " + (yycolumn+1) + " " + yychar);
    }
    
    protected void emit_error(String message){
    	System.out.println("scanner error: " + message + " at : 2" + 
    			(yyline+1) + " " + (yycolumn+1) + " " + yychar);
    }
%}

NovaLinha = [\n]
EspacoEmBranco = [ ]+ | [\t]

Digito = [0-9]
Inteiro = {Digito}+
Float = {Digito}+"."{Digito}+ | {Digito}+"." | "."{Digito}+
ConteudoString = [^\n\"]

Letra = [a-zA-Z]
Identificador = [:jletter:][:jletterdigit:]*

Comentario = {ComentarioDeLinha} | {ComentarioDeBloco}
ComentarioDeBloco = "/*"  ~"*/"
ComentarioDeLinha = "//" [^\n]* {NovaLinha}

%eofval{
    return symbolFactory.newSymbol("EOF",sym.EOF);
%eofval}

%state STRING

%%

<YYINITIAL>{

	{Inteiro}       {return symbolFactory.newSymbol("NUM_INTEIRO", NUM_INTEIRO);}
	{Float}         {return symbolFactory.newSymbol("NUM_FLOAT", NUM_FLOAT);}

	"int" 			{return symbolFactory.newSymbol("INT", INT);}
	"float" 		{return symbolFactory.newSymbol("FLOAT", FLOAT);}
	"bool" 			{return symbolFactory.newSymbol("BOOL", BOOL);}
	"string" 		{return symbolFactory.newSymbol("STRING", STRING);}
	"true" 			{return symbolFactory.newSymbol("TRUE", TRUE);}
	"false" 		{return symbolFactory.newSymbol("FALSE", FALSE);}
	"func" 			{return symbolFactory.newSymbol("FUNC", FUNC);}
	"if" 			{return symbolFactory.newSymbol("IF", IF);}
	"else" 			{return symbolFactory.newSymbol("ELSE", ELSE);}
	"for" 			{return symbolFactory.newSymbol("FOR", FOR);}
	"main"			{return symbolFactory.newSymbol("MAIN", MAIN);}
	"package"		{return symbolFactory.newSymbol("PACKAGE", PACKAGE);}
	"print"		    {return symbolFactory.newSymbol("PRINT", PRINT);}
	"println"		{return symbolFactory.newSymbol("PRINTLN", PRINTLN);}
    "return"		{return symbolFactory.newSymbol("RETURN", RETURN);}
	
	"(" 			{return symbolFactory.newSymbol("ABREPARENTESE", ABREPARENTESE);}
	")" 			{return symbolFactory.newSymbol("FECHAPARENTESE", FECHAPARENTESE);}
	"{" 			{return symbolFactory.newSymbol("ABRECHAVE", ABRECHAVE);}
	"}" 			{return symbolFactory.newSymbol("FECHACHAVE", FECHACHAVE);}
	"[" 			{return symbolFactory.newSymbol("ABRECOLCHETE", ABRECOLCHETE);}
	"]" 			{return symbolFactory.newSymbol("FECHACOLCHETE", FECHACOLCHETE);}
	
	"+" 			{return symbolFactory.newSymbol("MAIS", MAIS);}
	"&&" 			{return symbolFactory.newSymbol("AND", AND);}
	"==" 			{return symbolFactory.newSymbol("EQUAL", EQUAL);}
	"||" 			{return symbolFactory.newSymbol("OU", OU);}
	"-" 			{return symbolFactory.newSymbol("MENOS", MENOS);}
	"<" 			{return symbolFactory.newSymbol("MENORQ", MENORQ);}
	"<=" 			{return symbolFactory.newSymbol("MENORIGUAL", MENORIGUAL);}
	"*" 			{return symbolFactory.newSymbol("VEZES", VEZES);}
	"<-" 			{return symbolFactory.newSymbol("MENORSUB", MENORSUB);}
	">" 			{return symbolFactory.newSymbol("MAIORQ", MAIORQ);}
	">=" 			{return symbolFactory.newSymbol("MAIORIGUAL", MAIORIGUAL);}
	"/" 			{return symbolFactory.newSymbol("DIVISAO", DIVISAO);}
	"/=" 			{return symbolFactory.newSymbol("DIVISAOIGUAL", DIVISAOIGUAL);}
	"%=" 			{return symbolFactory.newSymbol("RESTOIGUAL", RESTOIGUAL);}
	"="  			{return symbolFactory.newSymbol("IGUAL", IGUAL);}
	":=" 			{return symbolFactory.newSymbol("ATRIBUICAO", ATRIBUICAO);}
	"!=" 			{return symbolFactory.newSymbol("DIFERENTE", DIFERENTE);}
	"," 		    {return symbolFactory.newSymbol("VIRGULA", VIRGULA);}
	";" 		    {return symbolFactory.newSymbol("PONTO_E_VIRGULA", PONTO_E_VIRGULA);}
	
	{Identificador} {return symbolFactory.newSymbol("IDENTIFICADOR", IDENTIFICADOR, yytext());}
	
	/* string literal*/
	\" 				{yybegin(STRING); string.setLength(0); }
	
	{Comentario} {/*Nao faz nada*/}
	{NovaLinha} {/*Nao faz nada*/}
	{EspacoEmBranco} {/*Nao faz nada*/}

	
}

<STRING> {
	\"					{yybegin(YYINITIAL); return symbolFactory.newSymbol("STRING_LITERAL", STRING_LITERAL); }
	
	{ConteudoString}+	{string.append(yytext());}
	
	// error fallback
	.|\n          { emit_warning("Unrecognized character '" +yytext()+"' -- ignored"); }
	
}



// error fallback
.|\n          { emit_warning("Unrecognized character '" +yytext()+"' -- ignored"); }
